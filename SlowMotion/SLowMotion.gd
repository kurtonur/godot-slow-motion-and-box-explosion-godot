extends Node

var isSlowTime: = false setget setIsSlow, getIsSlow
export(int,1, 10, 1) var slowMotionSpeed = 5
export(int,1, 20, 1) var slowTimer =  10

var timer = Timer.new()

func _ready():
	timer.connect("timeout",self,"_on_timer_timeout")
	timer.set_one_shot(true)
	add_child(timer)
	pass

func _physics_process(delta):
	
	if(isSlowTime):
		Engine.time_scale = max(Engine.time_scale -slowMotionSpeed * delta,0.1)
	else:
		Engine.time_scale = min(Engine.time_scale+slowMotionSpeed * delta,1)
	pass
	
func _on_timer_timeout():
	setIsSlow(false)
	pass
	
func setIsSlow(value):
	isSlowTime = value
	if isSlowTime and timer.get_time_left() == 0:
		timer.start(slowTimer*0.02)
	pass
	
func getIsSlow():
	return isSlowTime
	pass
	
