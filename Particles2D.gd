extends Particles2D


var time = 0
var start = false

func _process(delta):
	 if not is_emitting():
		  start = true
	 if start:
		  time += delta
	 if time >= get_lifetime():
		  queue_free()
