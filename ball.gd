extends RigidBody2D

func _process(delta):
	var bodies=get_colliding_bodies()
	
	for body in bodies:
		if( "box" in body.get_name() ):
			$"../SlowMotion".setIsSlow(true)
			var particle = preload("res://Particles2D.tscn").instance()
			particle.transform = body.transform
			particle.set_emitting(true)
			$"../".add_child(particle)
			body.queue_free()
		
	pass
	
func _input(event):
	if event is InputEventMouseButton:
		set_linear_velocity(Vector2(1200,0))
		pass
	pass
